import React from 'react'
import { Router, Route, IndexRoute, browserHistory } from 'react-router'

//Components
import Wrapper from './components/Wrapper'
import Rechercher from './components/Rechercher'
import Home from './components/Home'
import City from './containers/City'
import SiteMap from './containers/SiteMap'
import Index from './components/Index'

import NotFoundPage from './components/NotFoundPage';

export const routes = (
    <Route component={Wrapper}>
        <Route path='/' components={Index}>
            <IndexRoute component={Home}/>
            <Route path='rechercher-restaurant-roanne' component={Rechercher}/>
            <Route path='restaurant-:city' component={City}/>
            <Route path='sitemap' component={SiteMap}/>
            <Route path="*" component={NotFoundPage}/>
        </Route>
    </Route>
)

const router = ({ history }) => (
    <Router history={history}>
        { routes }
    </Router>
)

export default router