import React from 'react'
import { connect } from 'react-redux'
import SiteMapComponent from '../components/SiteMap'

import { getAllCity } from '../selectors/city'

const mapStateToProps = state => ({
    city: getAllCity(state),
})

export default connect(
    mapStateToProps,
)(SiteMapComponent)