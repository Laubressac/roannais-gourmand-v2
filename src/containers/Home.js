import React from 'react'
import { connect } from 'react-redux'
import HomeComponent from '../components/Home'

const mapStateToProps = state => ({

})

const mapDispatchToProps = dispatch => ({

})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HomeComponent)
