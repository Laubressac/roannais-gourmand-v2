import React, { Component } from 'react'
import { connect } from 'react-redux'
import { ALL_CITY_FETCH } from '../../actions'

class QueryAllCity extends Component {
    render () {
        return <noscript/>
    }

    /******************************** COMPONENT LIFECYCLE ********************************/
    componentWillMount () {
        this.request(this.props)
    }

    /******************************** CUSTOM METHODS ********************************/
    request (props) {
        const { fetchAllCity } = props
        fetchAllCity()
    }
}

const mapStateToProps = () => ({})

const mapDispatchToProps = dispatch => ({
    fetchAllCity: () => dispatch({ type: ALL_CITY_FETCH })
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(QueryAllCity)