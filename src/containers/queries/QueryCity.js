import React, { Component } from 'react'
import { connect } from 'react-redux'
import { CITY_FETCH } from '../../actions'

class QueryCity extends Component {
    render () {
        return <noscript/>
    }

    /******************************** COMPONENT LIFECYCLE ********************************/
    componentWillMount () {
        this.request(this.props)
    }

    /******************************** CUSTOM METHODS ********************************/
    request (props) {
        const { fetchCity } = props
        fetchCity()
    }
}


const mapDispatchToProps = dispatch => ({
    fetchCity: () => dispatch({ type: CITY_FETCH })
})

export default connect(
    null,
    mapDispatchToProps
)(QueryCity)