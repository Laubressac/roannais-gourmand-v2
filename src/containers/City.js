import React from 'react'
import { connect } from 'react-redux'
import CityComponent from '../components/City'
import { CITY_FETCH } from '../actions'

import { getCity } from '../selectors/city'

const mapStateToProps = state => ({
    city: getCity(state),
})

export default connect(
    mapStateToProps
)(CityComponent)