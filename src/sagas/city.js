import { call, put } from 'redux-saga/effects'
import { takeEvery } from 'redux-saga'
import { CITY_FETCH, CITY_FETCH_SUCCESS, CITY_FETCH_FAILED,
    ALL_CITY_FETCH, ALL_CITY_FETCH_SUCCESS, ALL_CITY_FETCH_FAILED
} from '../actions'
import { cityFetchApi, allCityFetchApi } from '../api/city'

export function* cityFetch (action) {
    try {
        if (action.payload == null) {
            const fetchRequest = yield call(cityFetchApi, null)
            if (fetchRequest) {
                yield put({type: CITY_FETCH_SUCCESS, payload: {city: null}})
            }
        } else {
            const fetchRequest = yield call(cityFetchApi, action.payload.byCity)
            if (fetchRequest) {
                yield put({type: CITY_FETCH_SUCCESS, payload: {city: fetchRequest}})
            }
        }
    } catch (error) {
        console.log(error)
    }
}

export function* allCityFetch () {
    try {
        const fetchRequest = yield call(allCityFetchApi)
        if (fetchRequest) {
            yield put({ type: ALL_CITY_FETCH_SUCCESS, payload: { allCity: fetchRequest } })
        }
    } catch (error) {
        console.log(error)
    }
}


function* watchCityFetch () {
    yield* takeEvery(CITY_FETCH, cityFetch)
}

function* watchAllCityFetch () {
    yield* takeEvery(ALL_CITY_FETCH, allCityFetch)
}


function* flow () {
    yield [
        watchCityFetch(),
        watchAllCityFetch(),
    ]
}

export default flow