import city from './city'
import { fork, call } from 'redux-saga/effects'
import { fetchUrl } from '../api/fetch'

function* root () {
  yield [
    fork(city),
  ]
}

export default root
