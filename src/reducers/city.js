import { CITY_FETCH, CITY_FETCH_SUCCESS, CITY_FETCH_FAILED,
    ALL_CITY_FETCH_SUCCESS
} from '../actions'
import {REHYDRATE} from 'redux-persist/constants'
import { apiURL } from '../api/config'

const city = (state = { status: null, message: null, city: null }, { type, payload }) => {
    switch (type) {
        // case REHYDRATE:
        //     var incoming = payload.allCity
        //     if (incoming) return {...state, ...incoming, specialKey: processSpecial(incoming.specialKey)}
        //     return state
        case CITY_FETCH:
            return { ...state, status: 'loading', message: null }
        case CITY_FETCH_SUCCESS:
            return { ...state, city: payload.city }
        case ALL_CITY_FETCH_SUCCESS:
            return { ...state, allCity: payload.allCity }
        // case REHYDRATE:
        //     console.log('3000')
        //     return { ...state, allCity: payload.allCity }
        // case '@@router/LOCATION_CHANGE':
        case CITY_FETCH_FAILED:
            return { ...state, message: null, status: null }
        default:
            return state
    }
}

export default city
