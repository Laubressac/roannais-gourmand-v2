'use strict';

import path from 'path';
import { Server } from 'http';
import Express from 'express';
import React from 'react';
import { renderToString } from 'react-dom/server';
import { match, RouterContext } from 'react-router';
import { routes } from './routes';
import NotFoundPage from './components/NotFoundPage';
import Helmet from 'react-helmet';
import { Provider } from 'react-redux'
import configureStore from './store/configureStore'
import { browserHistory } from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'

// initialize the server and configure support for ejs templates
const app = new Express();
const server = new Server(app);
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

// define the folder that will be used for static assets
app.use(Express.static(path.join(__dirname, 'static')));

// universal routing and rendering
app.get('*', (req, res) => {
    match({ routes, location: req.url }, (err, redirectLocation, renderProps) => {
            // in case of error display the error message
            if (err) {
                return res.status(500).send(err.message);
            }

            // in case of redirect propagate the redirect to the browser
            if (redirectLocation) {
                return res.redirect(302, redirectLocation.pathname + redirectLocation.search);
            }

            // generate the React markup for the current route
            let markup = '';
            let head = Helmet.rewind();

            let title = head.title.toString();
            let meta = head.meta.toString();
            let link = head.link.toString();

            if (renderProps) {
                // if the current route matched we have renderProps
                const appHistory = browserHistory
                const store = configureStore(appHistory)
                markup = renderToString(<Provider store={ store } key="provider"><RouterContext {...renderProps}/></Provider>);

            } else {
                // otherwise we can render a 404 page
                markup = renderToString(<NotFoundPage/>);
                res.status(404);
            }
        

        return res.render('index', { markup, title, meta, link });
        //
        //     let html = `
        //         <!doctype html>
        //             <html>
        //             <head>
        //                 ${head.title}
        //                 ${head.meta}
        //                 ${head.link}
        //                 <meta charset="utf-8" />
        //                 <link rel="shortcut icon" href="images/favicon.ico">
        //                 <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
        //                 <link href='http://fonts.googleapis.com/css?family=Roboto:100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>
        //                 <link href='https://www.google.com/fonts#UsePlace:use/Collection:Droid+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
        //                 <link href='https://www.google.com/fonts#UsePlace:use/Collection:Ubuntu:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
        //                 <link href='http://fonts.googleapis.com/css?family=Great+Vibes' rel='stylesheet' type='text/css'>
        //                 <link href='http://fonts.googleapis.com/css?family=Old+Standard+TT:400,400italic,700' rel='stylesheet' type='text/css'>
        //
        //                 <link rel="stylesheet" media="screen" href="js/bootstrap/bootstrap.min.css" type="text/css" />
        //                 <link rel="stylesheet" href="/js/mainmenu/menu.css" type="text/css" />
        //                 <link rel="stylesheet" href="/css/default.css" type="text/css" />
        //                 <link rel="stylesheet" href="/css/layouts.css" type="text/css" />
        //                 <link rel="stylesheet" href="/css/shortcodes.css" type="text/css" />
        //                 <link rel="stylesheet" href="/css/font-awesome/css/font-awesome.min.css">
        //                 <link rel="stylesheet" media="screen" href="/css/responsive-leyouts.css" type="text/css" />
        //                 <link rel="stylesheet" href="/js/masterslider/style/masterslider.css" />
        //                 <link rel="stylesheet" type="text/css" href="/css/Simple-Line-Icons-Webfont/simple-line-icons.css" media="screen" />
        //                 <link rel="stylesheet" href="/css/et-line-font/et-line-font.css">
        //                 <link href="js/owl-carousel/owl.carousel.css" rel="stylesheet">
        //             </head>
        //             <body>
        //                 <div id="root">${markup}</div>
        //                 <script src="/js/main.js" type="text/javascript"></script>
        //                 <script src="/js/universal/jquery.js" type="text/javascript"></script>
        //                 <script src="/js/bootstrap/bootstrap.min.js" type="text/javascript"></script>
        //                 <script src="/js/masterslider/jquery.easing.min.js" type="text/javascript"></script>
        //                 <script src="/js/masterslider/masterslider.min.js" type="text/javascript"></script>
        //                 <script type="text/javascript">
        //                     (function($) {
        //                         "use strict";
        //                         var slider = new MasterSlider();
        //                         // adds Arrows navigation control to the slider.
        //                         slider.control('arrows');
        //                         slider.control('bullets');
        //
        //                         slider.setup('masterslider' , {
        //                             width:1600,    // slider standard width
        //                             height:750,   // slider standard height
        //                             space:0,
        //                             speed:45,
        //                             layout:'boxed',
        //                             loop:true,
        //                             preload:0,
        //                             autoplay:true,
        //                             view:"parallaxMask"
        //                         });
        //
        //                     })(jQuery);
        //                 </script>
        //                 <script src="/js/mainmenu/customeUI.js"></script>
        //                 <script src="/js/mainmenu/jquery.sticky.js"></script>
        //                 <script src="/js/owl-carousel/owl.carousel.js"></script>
        //                 <script src="/js/owl-carousel/custom.js"></script>
        //                 <script src="/js/scrolltotop/totop.js"></script>
        //
        //                 <script src="/js/scripts/functions.js" type="text/javascript"></script>
        //
        //                 <script>
        //                     (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        //                                 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        //                             m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        //                     })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        //
        //                     ga('create', 'UA-86144541-1', 'auto');
        //                     ga('send', 'pageview');
        //
        //                 </script>
        //                 <script src="/js/bundle.js"></script>
        //             </body>
        //         </html>
        //     `;
        //
        //     /* write html, close connection */
        //     res.write(html);
        //     res.end();
        }
    );
});

// start the server
const port = process.env.PORT || 3000;
const env = process.env.NODE_ENV || 'production';
server.listen(port, err => {
    if (err) {
        return console.error(err);
    }
    console.info(`Server running on http://localhost:${port} [${env}]`);
});
