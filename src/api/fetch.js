import { apiURL } from './config'
import $ from 'jquery'
import { getCookie } from '../utils/cookie'
import request from './request'

export const getURL = (url) => {
    return new Promise((resolve, reject) => {
        request.get(`${apiURL}/${url}`).then(res => resolve(res.body, res.status));
    })
}
