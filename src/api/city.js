import { getURL, postURL } from './fetch'

export const cityFetchApi = city => {
    return getURL(`restaurantByCity/${city}`)
}

export const allCityFetchApi = () => {
    return getURL(`allRestaurantByCity`)
}