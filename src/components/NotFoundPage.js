'use strict';

import React from 'react';
import { Link } from 'react-router';
import Helmet from "react-helmet";

export default class NotFoundPage extends React.Component {
    render() {
        return (
            <div className="not-found">


                <h1>404</h1>
                <h2>Page not found!</h2>
                <p>
                    <Link to="/rechercher-restaurant-roanne" title="rechercher-restaurant-roanne">Rechercher un restaurant sur Roanne</Link>
                </p>
            </div>
        );
    }
}
