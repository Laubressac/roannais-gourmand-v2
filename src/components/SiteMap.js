'use strict';

import React, { Component } from 'react'
import ListAllCity from './ListAllCity'
import PaginatorTest from './PaginatorTest';
import Helmet from "react-helmet";

class SiteMap extends Component{
    constructor(props, context){
        super(props, context)
        this.state = {
            pagination: {
                page: 1,
                perPage: 2000,
            },
            city: props.city,
            internError: '',
        }
        // console.log('512')
        // console.log(props.city)
        context.router
        this.selectPage = this.selectPage.bind(this);
        this.goToLastPage = this.goToLastPage.bind(this);
        this.onPerPage = this.onPerPage.bind(this);
    }

    render () {
        var self = this;

        // console.log('513')
        // let first = ' test'
        // if (this.props.city != null) {
        //     first = this.props.city[0].title;
        // }

        const data = this.props.city || [];
        const pagination = self.state.pagination || {};
        const paginated = paginate(data, pagination);
        const pages = Math.ceil(data.length / Math.max(
                isNaN(pagination.perPage) ? 1 : pagination.perPage, 1)
        );

        let test = paginated.data.map((city, i) =>
            <ListAllCity city={city} key={i}/>
        )

        // console.log(first)

        return (

            <div>
                <Helmet
                    title="Les restaurants dans la région Roannaise "
                    meta={[
                        {name: "description", content: "Trouvez un restaurant dans la région roannaise"}
                    ]}
                    link={[
                        {rel: "canonical", href: "http://www.roannais-gourmand.fr/sitemap"},
                    ]}
                />
                <section>
                    <div className="header-inner two">
                        <div className="inner text-center">
                            <h4 className="title text-white uppercase old-standardtt">Sitemap</h4>
                            <h5 className="text-white uppercase">Les restaurants de la région Roannaise</h5>
                        </div>
                        <div className="overlay bg-opacity-5"></div>
                        <img src="http://placehold.it/1920x600" alt='Restaurant roanne' title='restaurant roanne' className="img-responsive"/> </div>
                </section>
                <div className="clearfix"></div>

                <section className="sec-padding">
                    <div className="container">
                        <div className="row">
                            <div className="col-xs-12">
                                <div className="title-line-8 red2"></div>
                                <h1 className="section-title-4 great-vibes text-center">Site Map</h1>
                            </div>
                            <div className="clearfix"></div>

                            {test}
                            <div className="clearfix"></div>
                            <PaginatorTest pagination={pagination} pages={pages}
                                           labels={{previous: '<', next: '>'}}
                                           onSelect={self.selectPage} />
                        </div>
                    </div>
                </section>
            </div>
        )
    }

    goToLastPage() {
        const state = this.state;
        const pagination = state.pagination || {};
        const pages = Math.ceil(this.props.city.length / pagination.perPage);

        this.selectPage(pages);
    }
    selectPage(page) {
        const state = this.state;
        const pagination = state.pagination || {};
        const pages = Math.ceil(this.props.city.length / pagination.perPage);

        pagination.page = Math.min(Math.max(page, 1), pages);

        this.setState({
            pagination: pagination
        });
    }
    onPerPage(event) {
        var pagination = this.state.pagination || {};

        pagination.perPage = parseInt(event.target.value, 10);

        this.setState({
            pagination: pagination
        });
    }
}

function paginate(data, o) {
    data = data || [];

    // adapt to zero indexed logic
    var page = o.page - 1 || 0;
    var perPage = o.perPage;

    var amountOfPages = Math.ceil(data.length / perPage);
    var startPage = page < amountOfPages ? page : 0;

    return {
        amount: amountOfPages,
        data: data.slice(startPage * perPage, startPage * perPage + perPage),
        page: startPage
    };

}

SiteMap.contextTypes = {
    router: React.PropTypes.object
}

export default SiteMap