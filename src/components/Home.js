'use strict';

import React, { Component } from 'react'
import Router from 'react-router'
import Helmet from "react-helmet";

export default class Home extends Component{
    constructor(props, context){
        super(props, context)
        this.state = {
            internError: '',
        }
        context.router
    }

    render () {
        var self = this;

        var divStyle = {
            top: '200px',
            left: '400px'
        };

        var divStyle2 = {
            top: '300px'
        };

        var divStyle3 = {
            top: '470px',
            left: '750px'
        };


        return (

            <div>
                <Helmet
                    title='Découvrez tous les restaurants de Roanne et sa région'
                    meta={[
                        {"name": "description", "content": "découvrez tous les restaurants de Roanne et sé région, réservez en ligne dans vos restaurants roannais préférés, découvrez le mag d'helloresto, avec les actualités complètes des restaurant roannais."}
                    ]}
                    link={[
                        {"rel": "canonical", "href": "http://www.roannais-gourmand.fr/"},
                    ]}
                />
                
                <div className="master-slider ms-skin-default" id="masterslider">

                    <div className="ms-slide slide-1" data-delay="9">
                        <div className="slide-pattern"></div>
                        <img src="../js/masterslider/blank.gif" data-src="../images/restaurant-roanne.jpg" alt="les restaurants de Roanne et sa région" title="les restaurants de Roanne et sa région" />


                        <h3 className="ms-layer text18"
                            style={divStyle}
                            data-type="text"
                            data-delay="500"
                            data-ease="easeOutExpo"
                            data-duration="1230"
                            data-effect="left(250)"> Bienvenue sur le site des  </h3>

                        <h3 className="ms-layer text19"
                            style={divStyle2}
                            data-type="text"
                            data-delay="1000"
                            data-ease="easeOutExpo"
                            data-duration="1230"
                            data-effect="right(250)"> Restaurants de Roanne</h3>

                        <a href="http://www.helloresto.fr" rel="nofollow" title="recherchez les restaurant de Roanne et sa région" className="ms-layer sbut7"
                           style={divStyle3}
                           data-type="text"
                           data-delay="2000"
                           data-ease="easeOutExpo"
                           data-duration="1200"
                           data-effect="scale(1.5,1.6)"> Accéder à la recherche des restaurants de Roanne</a>
                    </div>

                    <div className="ms-slide slide-2" data-delay="9">
                        <div className="slide-pattern"></div>
                        <img src="../js/masterslider/blank.gif" data-src="../images/restaurant-region-roannaise.jpg" alt="les restaurant de la région roannaise" title="les restaurant de la région roannaise"/>

                        <h3 className="ms-layer text19"
                            style={divStyle}
                            data-type="text"
                            data-delay="1000"
                            data-ease="easeOutExpo"
                            data-duration="1230"
                            data-effect="right(250)"> Savourez !</h3>


                        <a href="http://www.helloresto.fr" rel="nofollow" title="trouver les restaurant de Roanne et sa région" className="ms-layer sbut7"
                           style={divStyle2}
                           data-type="text"
                           data-delay="2000"
                           data-ease="easeOutExpo"
                           data-duration="1200"
                           data-effect="scale(1.5,1.6)"> Accéder à la recherche </a>

                    </div>

                    <div className="ms-slide slide-3" data-delay="9">
                        <div className="slide-pattern"></div>
                        <img src="../js/masterslider/blank.gif" data-src="../images/restaurants-roanne.jpg" alt="découvrez les restaurants de roanne et sa région" title="découvrez les restaurants de roanne et sa région"/>

                        <h3 className="ms-layer text19"
                            style={divStyle}
                            data-type="text"
                            data-delay="1000"
                            data-ease="easeOutExpo"
                            data-duration="1230"
                            data-effect="right(250)"> Réservez !</h3>

                        <h3 className="ms-layer text6 text-white"
                            style={divStyle2}
                            data-type="text"
                            data-effect="top(45)"
                            data-duration="2000"
                            data-delay="1500"
                            data-ease="easeOutExpo"> Réservez en ligne auprès des restaurants de la région roannaise </h3>

                        <a href="http://www.helloresto.fr" rel="nofollow" title="resrevez dans les restaurant de Roanne et sa région" className="ms-layer sbut7"
                           style={divStyle3}
                           data-type="text"
                           data-delay="2000"
                           data-ease="easeOutExpo"
                           data-duration="1200"
                           data-effect="scale(1.5,1.6)"> Accéder à la réservation </a>

                    </div>

                    <div className="ms-slide slide-4" data-delay="9">
                        <div className="slide-pattern"></div>
                        <img src="../js/masterslider/blank.gif" data-src="../images/restaurant-de-roanne.jpg" alt="recherchez votre restaurant sur roanne et sa région" title="recherchez votre restaurant sur roanne et sa région"/>
                        <h3 className="ms-layer text19"
                            style={divStyle}
                            data-type="text"
                            data-delay="1000"
                            data-ease="easeOutExpo"
                            data-duration="1230"
                            data-effect="right(250)"> Dégustez !</h3>

                        <h3 className="ms-layer text6 text-white"
                            style={divStyle2}
                            data-type="text"
                            data-effect="top(45)"
                            data-duration="2000"
                            data-delay="1500"
                            data-ease="easeOutExpo"> Dégustez toutes types de cuisine, traditionnel, gastronomique, spécialités ... etc. </h3>

                        <a href="http://www.helloresto.fr" rel="nofollow" title="voir les restaurant de Roanne et sa région" className="ms-layer sbut7"
                           style={divStyle3}
                           data-type="text"
                           data-delay="2000"
                           data-ease="easeOutExpo"
                           data-duration="1200"
                           data-effect="scale(1.5,1.6)"> Accéder à la recherche </a>
                    </div>
                </div>

                <div className="clearfix"></div>

                <section className="sec-padding">
                    <div className="container">
                        <div className="row">
                            <div className="col-xs-12">
                                <div className="title-line-8 red2"></div>
                                <h1 className="section-title-4 great-vibes text-center">Recherchez votre restaurant sur Roanne et sa région</h1>
                                <p className="sub-title text-center">Notre site Internet qui vous permet de rechercher votre restaurant sur Roanne et sa région, découvrez à la fois des restaurants de type gastronomique, traditionnel, spécialités ou encore les restaurants étoilés.
                                    <br/>Découvrez ci-dessous quelques exemple de restaurants que nous pouvons vous proposer sur Roanne et sa région.</p>
                            </div>
                            <div className="col-md-3 col-sm-6">
                                <div className="feature-box31 text-center bmargin">
                                    <div className="image-holder">
                                        <a href="http://www.helloresto.fr/restaurant-assiette-roannaise-137.html" target="_blank" title="restaurant l'assiette roannaise st forgeux Lespinasse">
                                            <img src="../images/restaurant-assiette-roannaise.jpg" alt="restaurant l'assiette roannaise st forgeux lespinasse" title="restaurant l'assiette roannaise st forgeux lespinasse" className="img-responsive"/>
                                        </a>
                                    </div>
                                    <div className="divider-line solid margin light"></div>
                                    <br/>
                                    <h4 className="old-standardtt uppercase"><a href="http://www.helloresto.fr/restaurant-assiette-roannaise-137.html" target="_blank" title="restaurant l'assiette roannaise">L'assiette roannaise</a></h4>
                                    <p>Découvrez le restaurant l'Assiette Roannaise situé à St Forgeux Lespinasse (42640), situé en plein coeur de la côté roannaise, le chef Christophe Guillon vous fera découvrir de belles saveurs.</p>
                                    <br/>
                                    <a className="btn btn-border light red-2" href="http://www.helloresto.fr/restaurant-assiette-roannaise-137.html" target="_blank" title="Découvrir le restaurant l'assiette roannaise">Découvrir l'Assiette Roannaise</a> </div>
                            </div>

                            <div className="col-md-3 col-sm-6">
                                <div className="feature-box31 text-center bmargin">
                                    <div className="image-holder">
                                        <a href="http://www.helloresto.fr/restaurant-l--atelier-rongefer-1274.html" target="_blank" title="restaurant l'atelier rongefer Charlieu">
                                            <img src="../images/restaurant-atelier-rongefer.jpg" alt="restaurant l'Atelier Rongefer Charlieu" className="img-responsive"/>
                                        </a>
                                    </div>
                                    <div className="divider-line solid margin light"></div>
                                    <br/>
                                    <h4 className="old-standardtt uppercase"><a href="http://www.helloresto.fr/restaurant-l--atelier-rongefer-1274.html" target="_blank" title="restaurant l'atelier rongefer">L'atelier Rongefer</a></h4>
                                    <p>En plein coeur de la ville de Charlieu, découvrez le restaurant l'Atelier Rongefer. Fabien Gauthier en cuisine vous fera découvrir les différentes saveurs de sa carte.</p>
                                    <br/>
                                    <a className="btn btn-border light red-2" href="http://www.helloresto.fr/restaurant-l--atelier-rongefer-1274.html" target="_blank" title="Découvrir le restaurant l'atelier rongefer">Découvrir l'Atelier Rongefer</a> </div>
                            </div>

                            <div className="col-md-3 col-sm-6">
                                <div className="feature-box31 text-center bmargin">
                                    <div className="image-holder">
                                        <a href="http://www.helloresto.fr/restaurant-restaurant-de-la-loire-120.html" target="_blank" title="restaurant de la Loire pouilly sous charlieu">
                                            <img src="../images/restaurant-de-la-loire.jpg" alt="restaurant de la Loire Pouilly sous Charlieu" title="restaurant de la Loire Pouilly sous Charlieu" className="img-responsive"/>
                                        </a>
                                    </div>
                                    <div className="divider-line solid margin light"></div>
                                    <br/>
                                    <h4 className="old-standardtt uppercase"><a href="http://www.helloresto.fr/restaurant-restaurant-de-la-loire-120.html" target="_blank" title="restaurant de la Loire">Restaurant de la Loire</a></h4>
                                    <p> Brigitte et Alain Rousseau vous accueillent au Restaurant de la Loire à Pouilly sous Charlieu. Une cuisine, des saveurs authentiques, tout est fait maison (pain, glaces, saumon fumé..)</p>
                                    <br/>
                                    <a className="btn btn-border light red-2" href="http://www.helloresto.fr/restaurant-restaurant-de-la-loire-120.html" target="_blank" title="Découvrir le restaurant de la Loire">Découvrir Restaurant de la Loire</a> </div>
                            </div>

                            <div className="col-md-3 col-sm-6">
                                <div className="feature-box31 text-center bmargin">
                                    <div className="image-holder">
                                        <a href="http://www.helloresto.fr/restaurant-le-petit-prince-132.html" target="_blank" alt="restaurant le Petit Prince à St Alban les eaux">
                                            <img src="../images/restaurant-le-petit-prince.jpg" alt="restaurant le Petit Prince à St Alban les eaux" title="restaurant le Petit Prince à St Alban les eaux" className="img-responsive"/>
                                        </a>
                                    </div>
                                    <div className="divider-line solid margin light"></div>
                                    <br/>
                                    <h4 className="old-standardtt uppercase"><a href="http://www.helloresto.fr/restaurant-le-petit-prince-132.html" target="_blank" title="restaurant le petit Prince">Le Petit Prince</a></h4>
                                    <p> Découvrez en plein coeur de la côté roannaise, le restaurant le petit Prince, situé sur la commune de St Alban les Eaux, vous découvrirez une cuisine gastronomique.</p>
                                    <br/>
                                    <a className="btn btn-border light red-2" href="http://www.helloresto.fr/restaurant-le-petit-prince-132.html" target="_blank" title="Découvrir le restaurant le Petit Prince">Découvrir le Petit Prince</a> </div>
                            </div>
                        </div>
                    </div>
                </section>
                <div className="clearfix"></div>

                <section className="parallax-section21">
                    <div className="section-overlay bg-opacity-5">
                        <div className="container sec-tpadding-3 sec-bpadding-3">
                            <div className="row">
                                <div className="col-md-8 col-centered">
                                    <div className="title-line-8 white"></div>
                                    <h1 className="section-title-4 great-vibes text-center text-white">Reservation</h1>
                                    <p className="text-white">Sur notre site, vous pouvez réserver en ligne auprès des restaurateurs de la région roannaise </p>
                                    <br/>
                                    <br/>
                                    <a className="btn btn-white btn-xround" href="http://www.helloresto.fr" rel="nofollow">Accéder à la resèrvation en ligne</a> </div>
                            </div>
                        </div>
                    </div>
                </section>
                <div className="clearfix"></div>


                <section className="sec-padding">
                    <div className="container">
                        <div className="row">
                            <div className="col-xs-12">
                                <div className="title-line-8 red2"></div>
                                <h1 className="section-title-4 great-vibes text-center">Chèque Cado Gourmand</h1>
                                <p className="sub-title text-center">Découvrez également notre une boutique en ligne, achetez en ligne des chèques Cado Gourmand, valable dans tous les restaurants de la région roannaise.</p>
                            </div>
                            <div className="col-md-3 col-sm-6">
                                <div className="feature-box31 text-center bmargin">
                                    <div className="image-holder">

                                        <a href="http://www.boutique-helloresto.fr" title="Commande en ligne de chèque cadeau"><img src="../images/cheque-cadeau-restaurant.jpg" alt="chèque cadeaux restaurant" className="img-responsive"/> </a></div>
                                    <div className="divider-line solid margin light"></div>
                                    <div className="clearfix"></div>
                                    <br/>
                                    <h4 className="old-standardtt uppercase"><a href="http://www.boutique-helloresto.fr" title="Commande en ligne de chèques cadeaux">Chèques Cado Gourmand</a></h4>
                                    <p>Achetez des chèques Cado Gourmand, valables dans plus de 20 restaurants de la régio roannaise</p>
                                    <br/>
                                    <a className="btn btn-border light red-2" href="http://www.boutique-helloresto.fr" title="commander des chèques cadeaux pour les restaurants de Roanne">Commander</a> </div>
                            </div>

                            <div className="col-md-3 col-sm-6">
                                <div className="feature-box31 text-center bmargin">
                                    <div className="image-holder">

                                        <a href="http://www.boutique-helloresto.fr" title="chèque cadeaux à partir de 20 €"><img src="../images/cheques-cadeaux-restaurant.jpg" alt="" className="img-responsive"/></a> </div>
                                    <div className="divider-line solid margin light"></div>
                                    <div className="clearfix"></div>
                                    <br/>
                                    <h4 className="old-standardtt uppercase"><a href="http://www.boutique-helloresto.fr" title="chèque cadeaux de 20 à 250 €">A partir de 20 €</a></h4>
                                    <p>Commander les chèques Cado Gourmand, de 20 à 250 € vous choisissez la somme d'argent et la pêrsonne va le consommer dans le restaurant de son choix</p>
                                    <br/>
                                    <a className="btn btn-border light red-2" href="http://www.boutique-helloresto.fr" title="Acheter des chèques cadeaux des restaurants de Roanne">Commande en ligne</a> </div>
                            </div>

                            <div className="col-md-3 col-sm-6">
                                <div className="feature-box31 text-center bmargin">
                                    <div className="image-holder">
                                        <a href="http://www.boutique-helloresto.fr" title="chèque cadeau des restaurants roannais"><img src="../images/restaurant-roannais.jpg" alt="chèques cadeaux des restaurants roannais" className="img-responsive"/> </a></div>
                                    <div className="divider-line solid margin light"></div>
                                    <div className="clearfix"></div>
                                    <br/>
                                    <h4 className="old-standardtt uppercase"><a href="http://www.boutique-helloresto.fr" title="Cado Gourmand ! le chèque cadeau des restaurants roannais">Cado Gourmand</a></h4>
                                    <p>C'est le cadeau original et gourmet qui ravie les papilles et satisfait à coup sûr quelles que soient les occasions.</p>
                                    <br/>
                                    <a className="btn btn-border light red-2" href="http://www.boutique-helloresto.fr" title="le chèque cadeau des restaurants roannais">Acheter en ligne</a> </div>
                            </div>

                            <div className="col-md-3 col-sm-6">
                                <div className="feature-box31 text-center bmargin">
                                    <div className="image-holder">

                                        <a href="http://www.boutique-helloresto.fr" title="Acheter roannais avec le chèque cadeau pour les restaurants de Roanne"><img src="../images/cheque-cado-restaurant-roanne.jpg" alt="les chèques cadeaux des restaurants roannais" className="img-responsive"/> </a></div>
                                    <div className="divider-line solid margin light"></div>
                                    <div className="clearfix"></div>
                                    <br/>
                                    <h4 className="old-standardtt uppercase"><a href="http://www.boutique-helloresto.fr" title="Achetez en ligne les chèques cadeaux des restaurants de Roanne">Une offre complète</a></h4>
                                    <p>Achetez en ligne des cours oenologique, des cours de cuisine, des menus au restauranst de la région roannaise ... etc.</p>
                                    <br/>
                                    <a className="btn btn-border light red-2" href="http://www.boutique-helloresto.fr" title="Acheter roannais avec les chèques cadeaux des restaurants de Roanne">Acheter en ligne</a> </div>
                            </div>
                        </div>
                    </div>
                </section>
                <div className="clearfix"></div>

                <section className="parallax-section23">
                    <div className="section-overlay bg-opacity-5">
                        <div className="container sec-tpadding-3 sec-bpadding-3">
                            <div className="row">
                                <div className="col-xs-12">
                                    <div className="title-line-8 red2"></div>
                                    <h1 className="section-title-4 great-vibes text-center text-white">Le Mag des restaurants de Roanne</h1>
                                    <p className="sub-title text-center text-white">Restez informé des actualités des restaurants de la région roannaise avec notre magazine d'actualité sur les restaurants Roannais.</p>
                                </div>
                                <div className="clearfix"></div>
                                <div id="owl-demo2" className="owl-carousel">
                                    <div className="item">
                                        <div className="col-md-6 bmargin">
                                            <div className="feature-box33 bmargin">
                                                <div className="col-md-6 col-sm-6 nopadding">
                                                    <div className="image-holder">
                                                        <img src="../images/actualites-restaurants-roanne.jpg" className="img-responsive" alt="toutes les actus des restaurant de Roanne" title="les actualités des restaurant roannais"/> </div>
                                                </div>
                                                <div className="col-md-6 col-sm-6 nopadding">
                                                    <div className="text-box-inner text-left">
                                                        <h4 className="old-standardtt uppercase"><a href="http://www.helloresto.fr/?-Le-Mag-d-helloresto-&actu" title="Les actualités des restaurants roannais">Les Actualités</a></h4>
                                                        <p>Découvrez toutes les actualités des restaurants de la région roannaise </p>
                                                        <p>Soirées spéciales, menus spéciaux, toutes les actualités</p>
                                                        <br/>
                                                        <a className="read-more" href="http://www.helloresto.fr/?-Le-Mag-d-helloresto-&actu" title="Découvrir les actualités des restaurants de roanne et sa région"><i className="fa fa-long-arrow-right"></i> Découvrir </a> </div>
                                                </div>
                                            </div>
                                            <div className="divider-line solid red2 top-padding"></div>
                                        </div>

                                        <div className="col-md-6 bmargin">
                                            <div className="feature-box33 bmargin">
                                                <div className="col-md-6 col-sm-6 nopadding">
                                                    <div className="image-holder">
                                                        <a href="http://www.helloresto.fr/-Le-Mag-d-helloresto-.html" title="le magazine des restaurants de roanne"><img src="../images/actualite-restaurant-roanne.jpg" className="img-responsive" alt="le magazine des restaurants de roanne"/> </a></div>
                                                </div>
                                                <div className="col-md-6 col-sm-6 nopadding">
                                                    <div className="text-box-inner text-left">
                                                        <h4 className="old-standardtt uppercase"><a href="#">Le Magazine</a></h4>
                                                        <p>Retrouvez tous les reportages des restaurants !!! </p>
                                                        <p>Reportages, actualités, coulisses des restaurants de Roanne et sa région ... etc</p>
                                                        <br/>
                                                        <a className="read-more" href="#"><i className="fa fa-long-arrow-right"></i> En savoir plus</a> </div>
                                                </div>
                                            </div>
                                            <div className="divider-line solid red2 top-padding"></div>
                                        </div>
                                    </div>

                                    <div className="item">
                                        <div className="col-md-6 bmargin">
                                            <div className="feature-box33 bmargin">
                                                <div className="col-md-6 col-sm-6 nopadding">
                                                    <div className="image-holder">
                                                        <a href="http://www.helloresto.fr/?-Le-Mag-d-helloresto-&recette" title="les recettes des chefs de la région roannaise"><img src="../images/recette-restaurant-roanne.jpg" className="img-responsive" alt="les recettes des restaurants de la région roannaise"/> </a></div>
                                                </div>
                                                <div className="col-md-6 col-sm-6 nopadding">
                                                    <div className="text-box-inner text-left">
                                                        <h4 className="old-standardtt uppercase"><a href="http://www.helloresto.fr/?-Le-Mag-d-helloresto-&recette" title="recette restaurants Roanne">Recettes</a></h4>
                                                        <p>Découvrez toutes les recettes de nos chefs locaux !</p>
                                                        <p>Recettes 100 % locales avec nos restaurants partenaires de la région roannaise</p>
                                                        <br/>
                                                        <a className="read-more" href="http://www.helloresto.fr/?-Le-Mag-d-helloresto-&recette" title="les recettes de chefs roannais"><i className="fa fa-long-arrow-right"></i> Découvrir</a> </div>
                                                </div>
                                            </div>
                                            <div className="divider-line solid red2 top-padding"></div>
                                        </div>
                                        <div className="clearfix"></div>
                                        <br/>
                                        <br/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <div className="row">
                    <div className="col-xs-12">
                        <div className="title-line-8 red2"></div>
                        <h2 className="section-title-4 great-vibes text-center">Restaurant Roanne</h2>
                        <p className="sub-title text-center">
                            A Roanne Troisgros a fait des petits ! C'est ce qu'on dit sur la région, car effectivement, bon nombre de nos restaurants à Roanne et aux alentours sortent de la maison Troisgros !
                            Nous possédons dans notre belle région gastronomique une multitude de cuisine, qui rend délicieux notre beau paysage culinaire.
                            Sur Helloresto, nous allons vous donner la bonne recette pour apprécier ce  patrimoine culinaire et rechercher votre restaurants à Roanne !
                        </p>
                        <br/>
                        <h2 className="section-title-4 great-vibes text-center">Comment rechercher votre restaurant à Roanne</h2>
                        <p className="sub-title text-center">
                            Pour commencer, nous allons vous faire découvrir différents types de cuisines des restaurants de Roanne et sa région, un délice pour les papilles. Pourquoi pas s'arrêter vers des restaurants spécialistes en viande charolaise, une auberge, des restaurants avec des cuisines de type traditionnelles, brasserie, pizzéria, voir même des restaurant de type gastronomique pour les plus gourmands !
                            Envi d'un repas de qualité ! En groupe en famille, en amoureux ou un repas d'entreprise, Helloresto regroupe tous ces critères pour rechercher votre restaurant à Roanne.
                            Grâce à un puissant moteur de recherche sur Helloresto, vous trouverez les meilleurs restaurants de la région roannaise avec tous les critères de recherches culinaires que vous souhaitez.
                        </p>
                        <br/>
                        <h2 className="section-title-4 great-vibes text-center">Découvrir les menus des restaurants de Roanne</h2>
                        <p className="sub-title text-center">
                            Besoin d'un menu ? Sur Helloresto, vous trouverez les liens des sites internet des restaurants Roannais, vous permettant de découvrir l'intégralité des menus que chaque chef aura établi dans son restaurant.
                            Le petit plus : le visuel, vous découvrirez de splendides photos culinaires mettant en valeur les plats de nos chefs cuisinier préférés, mais également les salles de chaque restaurant roannais.
                        </p>
                        <br/>
                        <h2 className="section-title-4 great-vibes text-center">Toutes les actus des restaurants de la région roannaise</h2>
                        <p className="sub-title text-center">
                            Des actus, des actus ! Helloresto vous propose de succulentes et exquises actualités de tous les restaurants de Roanne et ses environs.
                            Découvrir le menu du jour du Tourdion, la spécialité de la costelloise ou encore les bons plans des restaurateurs qui proposent un budget raisonnable, savoir quels sont les jours d'ouverture et les horaires du restaurant ... bref, nous vous apportons toutes les nouveautés et les coulisses de la restauration en générale sur notre beau territoire.
                            <br/>
                            Les producteurs et produits locaux mis à l'honneur ! On sait que la Loire (et particulièrement le Roannais) regorge de produits du terroir exceptionnels, vous découvrirez des focus et des coups de cœur sur nos producteurs locaux et sur nos restaurateurs de Roanne et sa région.
                        </p>
                        <br/>
                        <h2 className="section-title-4 great-vibes text-center">Où dormir sur le roannais</h2>
                        <p className="sub-title text-center">
                            Où dormir, la question que nous nous posons tous lorsque l'on se déplace en voiture un peu loin de notre domicile en vacance ou tout simplement en week-end gourmand ou week-end gastronomique !
                            Rassurez-vous, Helloresto à pensé à tout ! Vous découvrirez également tous les hôtel restaurants de la région, donc n'hésitez plus !
                            <br/><br/>
                            Bref, sur helloresto, les restos, on en connaît un rayon !
                            <br/><br/>
                            N'hésitez pas à rechercher votre restaurant sur Roanne et ses environs !
                        </p>
                    </div>
                </div>
            </div>
        )
    }

}

Home.contextTypes = {
  router: React.PropTypes.object
}
