import React from 'react'
import QueryCity from '../containers/queries/QueryCity'
import QueryAllCity from '../containers/queries/QueryAllCity'
import { getCookie } from '../utils/cookie'

const Index = ({ children }) => {
	return (
		<div className="Index">
			<QueryCity/>
			<QueryAllCity/>
			{ children }
		</div>
	)
}

export default Index
