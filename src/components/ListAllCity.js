'use strict';

import React, { Component } from 'react';
import { Link } from 'react-router'

class ListAllCity extends Component {
    constructor(props, context){
        super(props, context)
    }

    render() {
        var self = this;
        const resto = self.props.city

        return (
            <div className="col-md-6 col-sm-12">
                <div className="feature-box34 bmargin">
                    <div className="text-box-right more-padding-3 text-center">
                        <h4 className="old-standardtt less-mar1">
                           <a href={resto.url} title={'restaurant '+resto.title+ ' à '+resto.city+ ' ('+resto.cp+') '+resto.adress} target="_blank"> Restaurant {resto.title}</a>
                        </h4>
                        <span>{resto.adress}</span>
                        <br/>
                        <span>{resto.cp + ' ' + resto.city}</span>
                        <br/>
                        Voir tous les <Link to={resto.urlCity} title={'restaurant '+resto.city}> restaurants à <strong>{resto.city}</strong></Link>
                        <br/>
                        Voir tous <Link to={'restaurant-'+resto.cp} title={'restaurant '+resto.cp}> les restaurant <strong>{resto.cp}</strong></Link>
                    </div>
                </div>
            </div>
        )
    }
}

export default ListAllCity
