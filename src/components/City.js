'use strict';

import React, { Component } from 'react'
import ReactDOMServer from "react-dom/server";
import ListCity from './ListCity'
import PaginatorTest from './PaginatorTest';
import { CITY_FETCH } from '../actions'
import Helmet from "react-helmet";

class City extends Component{
    constructor(props, context){
        super(props, context)
        this.state = {
            pagination: {
                page: 1,
                perPage: 10,
            },
            internError: '',
            byCity: this.props.params.city
        }
        context.router
        this.selectPage = this.selectPage.bind(this);
        this.goToLastPage = this.goToLastPage.bind(this);
        this.onPerPage = this.onPerPage.bind(this);

        console.log(this.state.byCity)
        const byCity = this.state.byCity
        const { oneCity, dispatch } = this.props
        dispatch({type: CITY_FETCH, payload: {byCity}})
    }

    render () {
        var self = this;


        const data = this.props.city || [];
        const byCity = this.props.params.city || []
        const pagination = self.state.pagination || {};
        const paginated = paginate(data, pagination);
        const pages = Math.ceil(data.length / Math.max(
                isNaN(pagination.perPage) ? 1 : pagination.perPage, 1)
        );


        const byCity2 = 'Les restaurant à '+byCity;
        const canonical = 'http://www.roannais-gourmand.fr/restaurant-'+byCity;
        return (
            <div>
                <Helmet
                    title={'Les restaurants à '+byCity}
                    meta={[
                        {'name': 'description', 'content': byCity2}
                    ]}
                    link={[
                        {'rel': 'canonical', 'href': canonical },
                    ]}
                    onChangeClientState={(newState) => console.log(newState)}
                />
                <section>
                    <div className="header-inner two">
                        <div className="inner text-center">
                            <h4 className="title text-white uppercase old-standardtt">Rechercher un restaurant</h4>
                            <h5 className="text-white uppercase">Les restaurants de {byCity}</h5>
                        </div>
                        <div className="overlay bg-opacity-5"></div>
                        <img src="http://placehold.it/1920x600" alt={'Restaurant de '+byCity} title={'restaurant de '+byCity} className="img-responsive"/> </div>
                </section>
                <div className="clearfix"></div>

                <section className="sec-padding">
                    <div className="container">
                        <div className="row">
                            <div className="col-xs-12">
                                <div className="title-line-8 red2"></div>
                                <h1 className="section-title-4 great-vibes text-center">Les restaurants de {byCity}</h1>
                                <p className="sub-title text-center">Réserver chez un restaurateur de '{byCity}'</p>
                            </div>
                            <div className="clearfix"></div>

                            {paginated.data.map((city, i) =>
                                <ListCity city={city} key={i}/>
                            )}
                            <div className="clearfix"></div>
                            <PaginatorTest pagination={pagination} pages={pages}
                                           labels={{previous: '<', next: '>'}}
                                           onSelect={self.selectPage} />
                        </div>
                    </div>
                </section>
            </div>
        )
    }

    goToLastPage() {
        const state = this.state;
        const pagination = state.pagination || {};
        const pages = Math.ceil(this.props.city.length / pagination.perPage);

        this.selectPage(pages);
    }
    selectPage(page) {
        const state = this.state;
        const pagination = state.pagination || {};
        const pages = Math.ceil(this.props.city.length / pagination.perPage);

        pagination.page = Math.min(Math.max(page, 1), pages);

        this.setState({
            pagination: pagination
        });
    }
    onPerPage(event) {
        var pagination = this.state.pagination || {};

        pagination.perPage = parseInt(event.target.value, 10);

        this.setState({
            pagination: pagination
        });
    }


}

function paginate(data, o) {
    data = data || [];

    // adapt to zero indexed logic
    var page = o.page - 1 || 0;
    var perPage = o.perPage;

    var amountOfPages = Math.ceil(data.length / perPage);
    var startPage = page < amountOfPages ? page : 0;

    return {
        amount: amountOfPages,
        data: data.slice(startPage * perPage, startPage * perPage + perPage),
        page: startPage
    };

}

City.contextTypes = {
    router: React.PropTypes.object
}

export default City
