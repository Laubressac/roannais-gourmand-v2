'use strict';

import React, { Component } from 'react';
import NotFoundPage from './NotFoundPage';

class ListCity extends Component {
    constructor(props, context){
        super(props, context)
    }

    render() {
        var self = this;
        const resto = self.props.city

        if (!resto) {
            return <NotFoundPage/>;
        }

        // console.log(resto)
        return (
            <div className="col-md-6 col-sm-12">
                <div className="feature-box34 bmargin">
                    <div className="image-holder">
                        <img src={resto.img} alt={resto.title} width="120" height="120" className="img-responsive"/>
                    </div>
                    <div className="text-box-right more-padding-3 text-center">
                        <h4 className="old-standardtt less-mar1">
                            <a href={resto.url} title={'restaurant '+resto.title+ ' à '+resto.city+ ' ('+resto.cp+') '+resto.adress} target="_blank"> Restaurant {resto.title}</a>
                        </h4>
                        <span>{resto.adress}</span><br/>
                        <span>{resto.cp + ' ' + resto.city}</span><br/>
                    </div>
                </div>
            </div>
        )
    }
}

export default ListCity
