'use strict';

import React, { Component } from 'react'
import Helmet from "react-helmet";

class Rechercher extends Component{
    constructor(props, context) {
        super(props, context);
        this.name = ''
        this.state = {value: ''};
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        this.setState({value: event.target});
    }

    static get contextTypes() {
        return {
            router: React.PropTypes.object.isRequired,
        };
    }

    render() {
        return (
            <div>
                <Helmet
                    title="Rechercher un restaurant sur Roanne "
                    meta={[
                        {name: "description", content: "Trouvez un restaurant dans votre ville"}
                    ]}
                    link={[
                        {rel: "canonical", href: "http://www.roannais-gourmand.fr/rechercher-restaurant-roanne"},
                    ]}
                />
                <section>
                    <div className="header-inner two">
                        <div className="inner text-center">
                            <h4 className="title text-white uppercase old-standardtt">Rechercher un restaurant</h4>
                        </div>
                        <div className="overlay bg-opacity-5"></div>
                        <img src="http://placehold.it/1920x600" alt="" className="img-responsive"/> </div>
                </section>
                <div className="clearfix"></div>
                
                <section className="sec-padding">
                    <div className="container">
                        <div className="row">
                            <div className="col-xs-12">
                                <div className="title-line-8 red2"></div>
                                <h1 className="section-title-4 great-vibes text-center">Rechercher un restaurant par ville ou par code postal</h1>
                                <p className="sub-title text-center">Trouvez votre restaurant dans votre ville</p>
                            </div>
                            <div className="clearfix"></div>
                            <div className="col-md-6 bmargin"> <img src="/images/helloresto-rechercher.jpg" alt="" className="img-responsive"/> </div>

                            <div className="col-md-6">
                                <h3 className="old-standardtt">Rechercher</h3>
                                <br/>
                                <div className="res-form-holder bmargin">
                                    <form onSubmit={(e) => this.submit(e)}>
                                        <input type="text" id="city" name="city" placeholder="votre ville / votre CP" className="half-width" />
                                        <button className="btn-res" value="Rechercher">Rechercher</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }

    submit(e) {
        e.preventDefault()
        const city = document.querySelector('#city').value
        this.context.router.push('/restaurant-'+city);
    }
}


export default Rechercher