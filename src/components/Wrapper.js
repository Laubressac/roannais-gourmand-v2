'use strict';
import React from 'react';
import { Link, IndexLink } from 'react-router';

const Wrapper = ({ children }) => (
        <div className="wrapper-boxed">
            <div className="site_wrapper">
                <div className="topbar light topbar-padding">
                    <div className="container">
                        <div className="topbar-left-items">
                            <a href="http://www.helloresto.fr" target="_self" title="Recherchez votre restaurant sur Roanne et sa région">Recherchez votre restaurant sur Roanne et sa région</a>
                        </div>

                        <div className="topbar-right-items pull-right">
                            <ul className="toplist toppadding">

                                <li><h5 className="text-red-2 nopadding">
                                    <a href="http://www.helloresto.fr" rel="nofollow" target="_blank" title="découvrir les restaurants de Roanne">Découvrir les Restaurants de Roanne</a>
                                </h5></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div className="clearfix"></div>

                <div id="header">
                    <div className="container">
                        <div className="navbar yellow navbar-default red2 yamm">
                            <div className="navbar-header">
                                <button type="button" data-toggle="collapse" data-target="#navbar-collapse-grid" className="navbar-toggle">
                                    <span className="icon-bar"></span><span className="icon-bar"></span><span className="icon-bar"></span>
                                </button>
                                <IndexLink to="/" className="navbar-brand">
                                    <img src="images/logo.png" alt=""/>
                                </IndexLink>
                            </div>
                            <div id="navbar-collapse-grid" className="navbar-collapse collapse pull-right">
                                <ul className="nav red2 navbar-nav">
                                    <li> <IndexLink to="/" className="dropdown-toggle" title="les restaurants de Roanne">Restaurant Roanne et sa région</IndexLink></li>
                                    <li> <Link to="/rechercher-restaurant-roanne" title="rechercher-restaurant-roanne" className="dropdown-toggle">Rechercher un restaurant par ville</Link></li>
                                    <li> <Link to="/sitemap" title="les restaurant à roanne" className="dropdown-toggle">Sitemap</Link></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="clearfix"></div>

                { children }

                <div className="clearfix"></div>

                <section className="section-copyrights sec-moreless-padding">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12"> <span>Copyright © 2016 you'Nivers, <a href="http://www.younivers.fr" rel="nofollow" target="_blank" title="création de site Internet à Roanne" alt="création de site Internet à Roanne">création de site Internet à Roanne</a>, un concept helloresto, pour rechercher votre <a href="http://www.helloresto.fr" target="_blank" title="restaurant sur Roanne et sa région" alt="restaurant sur Roanne et sa région">restaurant sur roanne et sa région.</a></span></div>
                        </div>
                    </div>
                </section>
                <div className="clearfix"></div>

                <a href="#" className="scrollup red2"></a>
        </div>
    </div>
)

export default Wrapper
