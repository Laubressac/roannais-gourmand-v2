# Roannais Gourmand Test V2
Projet test : petit annuaire de restaurant sur Roanne et sa région

Application en React lié a symfony 3 pour récupérer les données. (API REST)

##Installation :

  Clone the project
  `git clone https://Laubressac@bitbucket.org/Laubressac/roannais-gourmand-v2.git`

  Open the folder
  `cd roannais-gourmand-v2`

  Install node modules:
  `npm install`

  build project:
  `npm run build`

  Run project:
  `npm run start`

  Open the following link in your browser:
  `http://localhost:3000`


![]()

